//
//  TumblrAPIDataManager.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 20/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "TumblrAPIDataManager.h"
#import "Post.h"
#import "PostMedia.h"

static NSString *const kTDRTumblrAPIBaseURLFormat = @"https://%@.tumblr.com/api/read/json/";

static NSString *const kTDRTumblrAPIRequestDebugParam = @"debug";
// debug=1 so it's just JSON, not javascript with JSON.
static NSUInteger const kTDRTumblrAPIRequestDebugTrueParamValue = 1;
static NSString *const kTDRTumblrAPIRequestFilterParam = @"filter";
static NSString *const kTDRTumblrAPIRequestFilterTextParamValue = @"text";
static NSString *const kTDRTumblrAPIRequestNumberOfEntriesToReturnParam = @"num";
static NSInteger const kTDRTumblrAPIRequestNumberOfEntriesToReturnValue = 50;
static NSString *const kTDRTumblrAPIRequestStartIndexParam = @"start";


static NSString *const kTDRTumblrAPIResponseInfoKey = @"tumblelog";
static NSString *const kTDRTumblrAPIResponseInfoTitleKey = @"title";
static NSString *const kTDRTumblrAPIResponseInfoDescriptionKey = @"description";
static NSString *const kTDRTumblrAPIResponseFirstPostIdxKey = @"posts-start";
static NSString *const kTDRTumblrAPIResponseTotalPostsCountKey = @"posts-total";
static NSString *const kTDRTumblrAPIResponsePostsListKey = @"posts";
static NSString *const kTDRTumblrAPIResponsePostURLKey = @"url";
static NSString *const kTDRTumblrAPIResponsePostTypeKey = @"type";

static NSString *const kTDRTumblrAPIResponsePostTypePhotoValue = @"photo";
static NSString *const kTDRTumblrAPIResponsePostTypeLinkValue = @"link";
static NSString *const kTDRTumblrAPIResponsePostTypeQuoteValue = @"quote";
static NSString *const kTDRTumblrAPIResponsePostTypeChatValue = @"chat";
static NSString *const kTDRTumblrAPIResponsePostTypeVideoValue = @"video";
static NSString *const kTDRTumblrAPIResponsePostTypeAudioValue = @"audio";
static NSString *const kTDRTumblrAPIResponsePostTypeRegularValue = @"regular";

static NSString *const kTDRTumblrAPIResponsePostUnixTimestampKey = @"unix-timestamp";

static NSString *const kTDRTumblrAPIResponsePostLinkText = @"link-text";
static NSString *const kTDRTumblrAPIResponsePostLinkURL = @"link-url";

static NSString *const kTDRTumblrAPIResponsePostPhotoCaptionKey = @"photo-caption";
static NSString *const kTDRTumblrAPIResponsePostPhotosKey = @"photos";
static NSString *const kTDRTumblrAPIResponsePostPhotosThumbnailURL = @"photo-url-75";
static NSString *const kTDRTumblrAPIResponsePostPhotosMediumURL = @"photo-url-400";

static NSString *const kTDRTumblrAPIResponsePostRegularTitleKey = @"regular-title";
static NSString *const kTDRTumblrAPIResponsePostRegularBodyKey = @"regular-body";

@implementation TumblrAPIDataManager

+ (PostType)postTypeForString:(NSString *)postTypeString {
    return (PostType)[@{
                        kTDRTumblrAPIResponsePostTypePhotoValue : @(PostTypePhoto),
                        kTDRTumblrAPIResponsePostTypeLinkValue  : @(PostTypeLink),
                        kTDRTumblrAPIResponsePostTypeRegularValue : @(PostTypeRegular),
                        }[postTypeString] integerValue];
}

+ (Post *)postFromDictionary:(NSDictionary *)dict {
    if (!dict) {
        return nil;
    }
    NSString *postTypeString = dict[kTDRTumblrAPIResponsePostTypeKey];
    PostType postType = [TumblrAPIDataManager postTypeForString:postTypeString];
    
    NSString *postURL = dict[kTDRTumblrAPIResponsePostURLKey];
    NSUInteger postUnixTimestamp = [dict[kTDRTumblrAPIResponsePostUnixTimestampKey] integerValue];
    
    Post *post = [Post new];
    NSString *postTitle;
    NSArray <PostMedia *> *postMedia;
    
    switch (postType) {
        case PostTypeLink: {
            postTitle = dict[kTDRTumblrAPIResponsePostLinkText];
            PostMedia *media = [PostMedia new];
            media.mediaURL = dict[kTDRTumblrAPIResponsePostLinkURL];
            media.mediaType = MediaTypeLink;
            postMedia = @[media];
            post.postSummaryText = media.mediaURL;
            break; }
        case PostTypePhoto: {
            postTitle = dict[kTDRTumblrAPIResponsePostPhotoCaptionKey];
            NSMutableArray *photos = [NSMutableArray new];
            for (NSDictionary *photoDict in dict[kTDRTumblrAPIResponsePostPhotosKey]) {
                PostMedia *media = [PostMedia new];
                media.mediaType = MediaTypeImage;
                media.mediaURL = photoDict[kTDRTumblrAPIResponsePostPhotosMediumURL];
                media.thumbnailMediaURL = photoDict[kTDRTumblrAPIResponsePostPhotosThumbnailURL];
                [photos addObject:media];
            }
            postMedia = [photos copy];
            break; }
        case PostTypeRegular: {
            postTitle = dict[kTDRTumblrAPIResponsePostRegularTitleKey];
            post.postSummaryText = dict[kTDRTumblrAPIResponsePostRegularBodyKey];
            break; }
        case PostTypeUndefined:
            break;
    }
    post.unixTimestamp = postUnixTimestamp;
    post.postURL = postURL;
    post.postType = postType;
    post.postTitle = postTitle;
    post.postMedia = postMedia;
    return post;
}

- (void)fetchPostsForUsername:(NSString *)username
       startingFromPostNumber:(NSUInteger)startingPostNumber
               withCompletion:(DMPostsListBlock)completion
                      failure:(DMFailureBlock)failure {
    // check if username is valid...
    NSString *escapedName = [username stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (![escapedName isEqualToString:username]) {
        // something's wrong...
        NSError *error = [NSError errorWithDomain:@"" code:404
                                         userInfo:@{NSLocalizedDescriptionKey : [NSString stringWithFormat:@"%@ doesn't seem like a valid Tumblr name", username]}];
        CALL(failure, error);
        return;
    }
    
    NSMutableSet *contentTypes = [self.responseSerializer.acceptableContentTypes mutableCopy];
    [contentTypes addObject:@"application/javascript"];
    self.responseSerializer.acceptableContentTypes = [contentTypes copy];
    self.responseSerializer.stringEncoding = NSUnicodeStringEncoding;
    
    NSString *urlString = [NSString stringWithFormat:kTDRTumblrAPIBaseURLFormat, username];

    NSDictionary *params = @{
                             kTDRTumblrAPIRequestDebugParam : @(kTDRTumblrAPIRequestDebugTrueParamValue),
                             kTDRTumblrAPIRequestFilterParam : kTDRTumblrAPIRequestFilterTextParamValue,
                             kTDRTumblrAPIRequestNumberOfEntriesToReturnParam : @(kTDRTumblrAPIRequestNumberOfEntriesToReturnValue),
                             kTDRTumblrAPIRequestStartIndexParam : @(startingPostNumber),
                             };
    
    [self GET:urlString
   parameters:params
     progress:^(NSProgress *downloadProgress) {
         
     }
      success:^(NSURLSessionDataTask *task, NSDictionary *responseDictionary) {
          // Process posts
          NSArray *postsArray = [responseDictionary objectForKey:kTDRTumblrAPIResponsePostsListKey];
          NSError *error = nil;
          CHECK_CONDITION_AND_CALL_BLOCK_WITH_ERROR_IF_FAILED(postsArray != nil, kTDRTumblrAPIResponsePostsListKey, &error, CALL(failure, error));
          NSMutableArray *posts = [NSMutableArray arrayWithCapacity:postsArray.count];
          for (NSDictionary *postDictionary in postsArray) {
              [posts addObject:[TumblrAPIDataManager postFromDictionary:postDictionary]];
          }
          CALL(completion, [posts copy]);
      }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
          CALL(failure, error);
      }];
}

@end
