//
//  DataManagerInterface.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 20/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

@class Post;

typedef void(^DMPostsListBlock)(NSArray <Post *>*posts);
typedef void(^DMFailureBlock)(NSError *error);

@protocol DataManagerInterface <NSObject>

- (void)fetchPostsForUsername:(NSString *)username
       startingFromPostNumber:(NSUInteger)startingPostNumber
               withCompletion:(DMPostsListBlock)completion
                      failure:(DMFailureBlock)failure;

@end
