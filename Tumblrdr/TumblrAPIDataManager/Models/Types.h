//
//  Types.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 20/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

typedef enum {
    PostTypeUndefined,
    PostTypePhoto,
    PostTypeLink,
    PostTypeRegular,
} PostType;

typedef enum {
    MediaTypeImage,
    MediaTypeLink,
    MediaTypeVideo,
} MediaType;
