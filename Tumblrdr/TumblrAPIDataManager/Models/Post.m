//
//  Post.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 20/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "Post.h"
#import "PostMedia.h"

@implementation Post

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ type %u: %@ (%@, #%lu media)", [self class], self.postType, self.postTitle, self.postURL, (unsigned long)self.postMedia.count];
}

@end
