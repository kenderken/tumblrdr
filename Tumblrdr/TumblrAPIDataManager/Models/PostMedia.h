//
//  PostMedia.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "Types.h"

@interface PostMedia : NSObject

@property (assign, nonatomic) MediaType mediaType;
@property (copy, nonatomic) NSString *mediaURL;
@property (copy, nonatomic) NSString *thumbnailMediaURL;

@end
