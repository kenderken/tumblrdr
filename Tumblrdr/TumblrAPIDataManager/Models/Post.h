//
//  Post.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 20/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "Types.h"

@class PostMedia;

@interface Post : NSObject

@property (assign, nonatomic) PostType postType;
@property (assign, nonatomic) NSUInteger unixTimestamp;
@property (copy, nonatomic) NSString *postURL;
@property (copy, nonatomic) NSString *postTitle;
@property (copy, nonatomic) NSString *postSummaryText;
@property (copy, nonatomic) NSArray <PostMedia *>*postMedia;

@end
