//
//  TumblrAPIDataManager.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 20/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

@import AFNetworking;

#import "DataManagerInterface.h"

@interface TumblrAPIDataManager : AFHTTPSessionManager <DataManagerInterface>

@end
