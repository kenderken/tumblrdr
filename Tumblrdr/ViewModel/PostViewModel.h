//
//  PostViewModel.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

@interface PostViewModel : NSObject

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;
@property (copy, nonatomic) NSURL *thumbPhoto;
@property (copy, nonatomic) NSString *genericIcon;
@property (copy, nonatomic) NSString *dateString;
@property (copy, nonatomic) NSArray <NSURL *>*imagesURLs;
@property (copy, nonatomic) NSString *postURL;

@end
