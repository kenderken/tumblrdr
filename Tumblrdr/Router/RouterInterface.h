//
//  RouterInterface.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BaseRouterInterface.h"

@class Post;

@protocol RouterInterface <BaseRouterInterface>

- (void)showMainScreen;
- (void)showDetailsScreenForPost:(Post *)post;
- (void)closeCurrentScreen;

- (void)openURLInExternalApplication:(NSURL *)URL;

@end
