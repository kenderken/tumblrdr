//
//  NavigationRouter.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "RouterInterface.h"

@class UINavigationController;

@interface NavigationRouter : NSObject <RouterInterface>

- (UINavigationController *)navigationController;

@end
