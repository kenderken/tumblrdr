//
//  NavigationRouter.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NavigationRouter.h"

#import "TimelineInteractor.h"
#import "TimelinePresenter.h"
#import "TimelineViewController.h"
#import "TumblrAPIDataManager.h"

#import "PostDisplayPresenter.h"
#import "PostDisplayViewController.h"

@interface NavigationRouter ()

@property (strong, nonatomic) UINavigationController *navigationController;

@end

@implementation NavigationRouter

- (BOOL)popToViewControllerOfClass:(Class)class animated:(BOOL)animated {
    if (!self.navigationController) {
        return NO;
    }
    // find first view controller on stack of class
    UIViewController *viewControllerToPopTo = nil;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:class]) {
            viewControllerToPopTo = vc;
            break;
        }
    }
    if (viewControllerToPopTo) {
        [self.navigationController popToViewController:viewControllerToPopTo animated:animated];
        return YES;
    } else {
        return NO;
    }
}

- (void)closeCurrentScreen {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showViewController:(UIViewController *)viewController {
    if (!self.navigationController) {
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    } else {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)showMainScreen {
    if (![self popToViewControllerOfClass:[TimelineViewController class] animated:YES]) {
        id<DataManagerInterface> dataManager = [[TumblrAPIDataManager alloc] init];
        id<TimelineInteractorInterface> interactor = [[TimelineInteractor alloc] initWithDataManagerInterface:dataManager];
        id<TimelinePresenterInterface> presenter = [[TimelinePresenter alloc] initWithRouterInterface:self
                                                                                  interactorInterface:interactor];
        TimelineViewController *viewController = [[TimelineViewController alloc] initWithPresenterInterface:presenter];
        [self showViewController:viewController];
    }
}

- (void)showDetailsScreenForPost:(Post *)post {
    NSLog(@"Show the details on the post: %@", post);
    id<PostDisplayPresenterInterface> presenter = [[PostDisplayPresenter alloc] initWithRouterInterface:self
                                                                                    interactorInterface:nil
                                                                                                   post:post];
    PostDisplayViewController *viewController = [[PostDisplayViewController alloc] initWithPresenterInterface:presenter];
    [self showViewController:viewController];
}


- (void)openURLInExternalApplication:(NSURL *)URL {
    NSLog(@"Opening: %@", URL);
    [[UIApplication sharedApplication] openURL:URL];
}

@end
