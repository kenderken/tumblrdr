//
//  TimelineInteractor.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "TimelineInteractor.h"

@interface TimelineInteractor ()

@property (strong, nonatomic, readonly) id<DataManagerInterface> dataManager;

@end

@implementation TimelineInteractor

- (instancetype)initWithDataManagerInterface:(id<DataManagerInterface>)dataManagerInterface {
    self = [self init];
    if (self) {
        _dataManager = dataManagerInterface;
    }
    return self;
}

#pragma mark - TimelineInteractorInterface

- (void)fetchPostsForUsername:(NSString *)username
               fromPostNumber:(NSUInteger)firstPostNumberToFetch
               withCompletion:(DMPostsListBlock)completion
                      failure:(DMFailureBlock)failure {
    [self.dataManager fetchPostsForUsername:username
                     startingFromPostNumber:firstPostNumberToFetch
                             withCompletion:completion
                                    failure:failure];
}

@end
