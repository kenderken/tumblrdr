//
//  TimelineInteractor.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "TimelineInteractorInterface.h"

@interface TimelineInteractor : NSObject <TimelineInteractorInterface>

- (instancetype)initWithDataManagerInterface:(id<DataManagerInterface>)dataManagerInterface;

@end
