//
//  TimelineInteractorInterface.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "DataManagerInterface.h"
#import "BaseInteractorInterface.h"

@protocol TimelineInteractorInterface <BaseInteractorInterface>

- (void)fetchPostsForUsername:(NSString *)username
               fromPostNumber:(NSUInteger)firstPostNumberToFetch
               withCompletion:(DMPostsListBlock)completion
                      failure:(DMFailureBlock)failure;

@end
