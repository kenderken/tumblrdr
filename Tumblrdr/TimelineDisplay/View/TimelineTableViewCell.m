//
//  TimelineTableViewCell.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>

#import "TimelineTableViewCell.h"
#import "PostViewModel.h"

@implementation TimelineTableViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.titleLabel.text = nil;
    self.subtitleLabel.text = nil;
    [self.imageView cancelImageDownloadTask];
    self.imageView.image = nil;
}

- (void)setImageURL:(NSURL *)imageURL {
    weakify(self);
    [self.mainImageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL]
                              placeholderImage:[UIImage imageNamed:@"placeholder"]
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                           strongify(self);
                                           self.mainImageView.image = image;
                                           [self layoutIfNeeded];
                                           // this will fit your cell's label
                                       }
                                       failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         
     }];
    
}

- (void)setViewModel:(PostViewModel *)viewModel {
    self.titleLabel.text = viewModel.title;
    self.subtitleLabel.text = viewModel.subtitle;
    self.mainImageView.image = [UIImage imageNamed:viewModel.genericIcon];
    self.dateLabel.text = viewModel.dateString;
    if (viewModel.thumbPhoto) {
        [self setImageURL:viewModel.thumbPhoto];
    }
}

@end
