//
//  TimelineViewInterface.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BaseViewInterface.h"

@protocol TimelineViewInterface <BaseViewInterface>

- (void)setScreenTitle:(NSString *)screenTitle;

- (void)showDialogAskingForUsernameWithCompletion:(void (^)(NSString *username))completion;

- (void)reloadTimeline;

@end
