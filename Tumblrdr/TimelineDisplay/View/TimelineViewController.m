//
//  TimelineViewController.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <AFNetworking/UIKit+AFNetworking.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

#import "TimelineViewController.h"
#import "TimelineTableViewCell.h"
#import "PostViewModel.h"

static CGFloat const kTLTableViewRowHeight = 100.0f;

@interface TimelineViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TimelineViewController

- (id<TimelinePresenterInterface>)timelinePresenter {
    return AS_PROTOCOL(TimelinePresenterInterface, self.presenter);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:CLASS_NAME_FOR_TYPE(TimelineTableViewCell) bundle:nil] forCellReuseIdentifier:CLASS_NAME_FOR_TYPE(TimelineTableViewCell)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"New name"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didTapNewName)];
    self.tableView.tableFooterView = [UIView new];
}

- (void)didTapNewName {
    [[self timelinePresenter] didPressAskNameAgainButton];
}

#pragma mark TimelineViewInterface

- (void)showDialogAskingForUsernameWithCompletion:(void (^)(NSString *))completion {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter Tumblr username"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    }];
    weakify(alert);
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * _Nonnull action) {
                                                   strongify(alert);
                                                   NSString *username = [[alert textFields] objectAtIndex:0].text;
                                                   CALL(completion, username);
                                               }];
    [alert addAction:ok];
    [self presentViewController:alert
                       animated:YES
                     completion:nil];
}

- (void)setScreenTitle:(NSString *)screenTitle {
    self.title = screenTitle;
}

- (void)reloadTimeline {
    [self.tableView reloadData];
}

@end

@interface TimelineViewController (TableViewDataSource) <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@end

@implementation TimelineViewController (TableViewDataSource)

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self timelinePresenter] numberOfTimelineRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TimelineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME_FOR_TYPE(TimelineTableViewCell) forIndexPath:indexPath];
    
    PostViewModel *viewModel = [[self timelinePresenter] postViewModelAtIndex:indexPath.row];
    [cell setViewModel:viewModel];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[self timelinePresenter] didTapOnRowAtIndex:indexPath.row];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row + 1 == [[self timelinePresenter] numberOfTimelineRows]) {
        [[self timelinePresenter] didReachEndOfList];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTLTableViewRowHeight;
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:@"No entries in timeline"];
}
@end
