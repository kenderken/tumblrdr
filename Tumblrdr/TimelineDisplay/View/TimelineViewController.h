//
//  TimelineViewController.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "TimelinePresenterInterface.h"
#import "TimelineViewInterface.h"

@interface TimelineViewController : BaseViewController <TimelineViewInterface>

@end
