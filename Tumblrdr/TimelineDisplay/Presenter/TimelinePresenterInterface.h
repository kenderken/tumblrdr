//
//  TimelinePresenterInterface.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BasePresenterInterface.h"

@class PostViewModel;

@protocol TimelinePresenterInterface <BasePresenterInterface>

- (void)didPressAskNameAgainButton;
- (void)didReachEndOfList;
- (void)didTapOnRowAtIndex:(NSUInteger)index;

- (NSUInteger)numberOfTimelineRows;
- (PostViewModel *)postViewModelAtIndex:(NSUInteger)index;

@end
