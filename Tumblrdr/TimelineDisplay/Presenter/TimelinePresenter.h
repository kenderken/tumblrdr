//
//  TimelinePresenter.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "TimelinePresenterInterface.h"
#import "TimelineInteractorInterface.h"
#import "RouterInterface.h"
#import "BasePresenter.h"

@interface TimelinePresenter : BasePresenter <TimelinePresenterInterface>

@end
