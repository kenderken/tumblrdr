//
//  TimelinePresenter.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "TimelinePresenter.h"
#import "TimelineViewInterface.h"
#import "UIViewController+Extensions.h"
#import "Post.h"
#import "PostMedia.h"
#import "PostViewModel.h"

@interface TimelinePresenter ()

@property (strong, nonatomic) NSArray <Post *>*posts;
@property (copy, nonatomic) NSString *username;
@property (assign, nonatomic) BOOL didReachEndOfTimeline;

@end

@implementation TimelinePresenter

- (id<TimelineViewInterface>)timelineViewInterface {
    return AS_PROTOCOL(TimelineViewInterface, self.view);
}

- (id<TimelineInteractorInterface>)timelineInteractor {
    return AS_PROTOCOL(TimelineInteractorInterface, self.interactor);
}

- (id<RouterInterface>)timelineRouter {
    return AS_PROTOCOL(RouterInterface, self.router);
}

- (void)fetchPostsFromPostNumber:(NSUInteger)firstPostNumber {
    weakify(self);
    [[self timelineInteractor] fetchPostsForUsername:self.username
                                      fromPostNumber:firstPostNumber
                                      withCompletion:^(NSArray<Post *> *posts) {
                                          strongify(self);
                                          if (posts.count == 0) {
                                              self.didReachEndOfTimeline = YES;
                                          }
                                          self.posts = [self.posts arrayByAddingObjectsFromArray:posts];
                                          [self reloadPostsView];
                                          [AS(UIViewController, [self timelineViewInterface]) hideFullScreenActivityIndicator];
                                      } failure:^(NSError *error) {
                                          [AS(UIViewController, [self timelineViewInterface]) showAlertWithError:error];
                                          [AS(UIViewController, [self timelineViewInterface]) hideFullScreenActivityIndicator];
                                          self.posts = nil;
                                          [self reloadPostsView];
                                          [[self timelineViewInterface] setScreenTitle:nil];
                                      }];
}

- (void)askForNameAndFetchItsTimeline {
    weakify(self);
    [AS(UIViewController, [self timelineViewInterface]) showFullScreenActivityIndicatorWithText:nil];
    [[self timelineViewInterface] showDialogAskingForUsernameWithCompletion:^(NSString *username) {
        strongify(self);
        self.username = username;
        self.didReachEndOfTimeline = NO;
        [[self timelineViewInterface] setScreenTitle:[NSString stringWithFormat:@"%@'s Timeline", self.username]];
        self.posts = @[];
        [self fetchPostsFromPostNumber:0];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self askForNameAndFetchItsTimeline];
}

- (void)reloadPostsView {
    [[self timelineViewInterface] reloadTimeline];
}

- (Post *)postAtIndex:(NSUInteger)index {
    return [self.posts safeObjectAtIndex:index];
}

#pragma mark - TimelinePresenterInterface

- (void)didPressAskNameAgainButton {
    [self askForNameAndFetchItsTimeline];
}

- (void)didReachEndOfList {
    if (!self.didReachEndOfTimeline) {
        [self fetchPostsFromPostNumber:self.posts.count];
    }
}

- (NSUInteger)numberOfTimelineRows {
    return self.posts.count;
}

- (NSString *)titleForPostAtIndex:(NSUInteger)index {
    Post *post = [self postAtIndex:index];
    
    NSString *title = post.postTitle;
    if (!title.length) {
        title = @"/Empty post title/";
    }
    title = [title stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    return title;
}

- (NSString *)summaryTextForPostAtIndex:(NSUInteger)index {
    return [self postAtIndex:index].postSummaryText;
}

- (NSString *)thumbnailURLStringAtIndex:(NSUInteger)index {
    Post *post = [self postAtIndex:index];
    NSArray <PostMedia *>*photoMedia = [post.postMedia filteredArrayPassingTest:^BOOL(PostMedia *obj) {
        if (obj.mediaType == MediaTypeImage) {
            return YES;
        }
        return NO;
    }];
    return [photoMedia firstObject].thumbnailMediaURL;
}

- (PostViewModel *)postViewModelAtIndex:(NSUInteger)index {
    PostViewModel *postViewModel = [PostViewModel new];
    Post *post = [self postAtIndex:index];
    
    NSDate *postDate = [NSDate dateWithTimeIntervalSince1970:post.unixTimestamp];
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterShortStyle;
    formatter.timeStyle = NSDateFormatterShortStyle;
    postViewModel.dateString = [formatter stringFromDate:postDate];
    postViewModel.title = post.postTitle.length ? [post.postTitle stringByReplacingOccurrencesOfString:@"\n" withString:@" "] : @"/Empty post title/";
    postViewModel.subtitle = post.postSummaryText;
    postViewModel.thumbPhoto = [NSURL URLWithString:[self thumbnailURLStringAtIndex:index]];
    // check type of photo
    if (postViewModel.thumbPhoto) {
        postViewModel.genericIcon = @"placeholder";
    } else {
        if ([post.postMedia filteredArrayPassingTest:^BOOL(PostMedia *obj) {
            return obj.mediaType == MediaTypeLink;
        }].count > 0) {
            postViewModel.genericIcon = @"link";
        } else if (post.postSummaryText.length > 0) {
            postViewModel.genericIcon = @"text";
        }
    }
    return postViewModel;
}

- (void)didTapOnRowAtIndex:(NSUInteger)index {
    [[self timelineRouter] showDetailsScreenForPost:[self.posts safeObjectAtIndex:index]];
}

@end
