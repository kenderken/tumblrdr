//
//  Helpers.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#define SAFE(__obj__) (__obj__ ? __obj__ : @"")

#define CALL(__block__, ...) (__block__ ? __block__(__VA_ARGS__) : nil)

#define CHECK_CONDITION_AND_CALL_BLOCK_WITH_ERROR_IF_FAILED(__condition__, __name__, __error__, __action__) \
if (!(__condition__)) { \
if ((__error__)) { \
*__error__ = [NSError errorWithDomain:@"Condition Error" code:500 userInfo:[NSDictionary dictionaryWithObject:__name__ forKey:@"condition"]]; \
} \
__action__; \
}

#define CLASS_NAME_FOR_TYPE(__type__) NSStringFromClass([__type__ class])

#define AS(__type__, __object__) ((__type__*)__object__)

#define AS_PROTOCOL(__protocol__, __object__) ((id<__protocol__>)__object__)

#define weakify(var) __weak typeof(var) AHKWeak_##var = var;

#define strongify(var) \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Wshadow\"") \
__strong typeof(var) var = AHKWeak_##var; \
_Pragma("clang diagnostic pop")

