//
//  NSArray+Extensions.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

@interface NSArray<__covariant ObjectType> (Extensions)

- (ObjectType)safeObjectAtIndex:(NSUInteger)index;

- (NSArray *)filteredArrayPassingTest:(BOOL (^)(ObjectType obj))test;
- (NSArray *)map:(id (^)(ObjectType obj))mapBlock;

@end
