//
//  UIViewController+Extensions.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "UIViewController+Extensions.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation UIViewController (Extensions)

- (UIAlertController *)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok"
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert
                       animated:YES
                     completion:nil];
    return alert;
}

- (UIAlertController *)showAlertWithError:(NSError *)error {
    return [self showAlertWithTitle:@"Error"
                            message:error.localizedDescription];
}

- (void)showFullScreenActivityIndicatorWithText:(NSString *)text {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view
                                              animated:YES];
    hud.labelText = text;
}

- (void)hideFullScreenActivityIndicator {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
