//
//  UIViewController+Extensions.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extensions)

- (UIAlertController *)showAlertWithError:(NSError *)error;
- (void)showFullScreenActivityIndicatorWithText:(NSString *)text;
- (void)hideFullScreenActivityIndicator;

@end
