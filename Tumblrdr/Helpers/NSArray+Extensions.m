//
//  NSArray+Extensions.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 21/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "NSArray+Extensions.h"

@implementation NSArray (SafeAccess)

- (id)safeObjectAtIndex:(NSUInteger)index {
    if (self.count <= index) {
        return nil;
    } else {
        return [self objectAtIndex:index];
    }
}

- (NSArray *)filteredArrayPassingTest:(BOOL (^)(id))test {
    NSIndexSet *indices = [self indexesOfObjectsPassingTest:^(id obj, NSUInteger idx, BOOL *stop) {
        return test(obj);
    }];
    NSArray *filtered = [self objectsAtIndexes:indices];
    return filtered;
}

- (NSArray *)map:(id (^)(id))mapBlock {
    NSMutableArray *mapped = [NSMutableArray arrayWithCapacity:self.count];
    for (id obj in self) {
        id mObj = mapBlock(obj);
        [mapped addObject:mObj];
    }
    return [mapped copy];
}

@end
