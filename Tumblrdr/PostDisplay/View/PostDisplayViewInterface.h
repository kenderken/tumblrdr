//
//  PostDisplayViewInterface.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BaseViewInterface.h"

@class PostViewModel;

@protocol PostDisplayViewInterface <BaseViewInterface>

- (void)displayViewModel:(PostViewModel *)viewModel;

@end
