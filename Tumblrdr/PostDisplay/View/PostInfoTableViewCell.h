//
//  PostInfoTableViewCell.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
