//
//  PostImageTableViewCell.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>

#import "PostImageTableViewCell.h"

@implementation PostImageTableViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self.mainImageView cancelImageDownloadTask];
    self.mainImageView.image = nil;
}

- (void)setImageURL:(NSURL *)imageURL {
    weakify(self);
    [self.mainImageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL]
                              placeholderImage:[UIImage imageNamed:@"placeholder"]
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                           strongify(self);
                                           self.mainImageView.image = image;
                                           [self layoutIfNeeded];
                                           // this will fit your cell's label
                                       }
                                       failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         
     }];
    
}

@end
