//
//  PostDisplayViewController.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "PostDisplayViewController.h"
#import "PostDisplayPresenterInterface.h"
#import "PostViewModel.h"
#import "PostInfoTableViewCell.h"
#import "PostImageTableViewCell.h"

static CGFloat const kPDTableViewPostImageRowHeight = 250.0f;
static CGFloat const kPDTableViewPostInfoRowHeightBase = 34.0f;

@interface PostDisplayViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) PostViewModel *viewModel;

@end

@implementation PostDisplayViewController

- (id<PostDisplayPresenterInterface>)postDisplayPresenter {
    return AS_PROTOCOL(PostDisplayPresenterInterface, self.presenter);
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerNib:[UINib nibWithNibName:CLASS_NAME_FOR_TYPE(PostInfoTableViewCell) bundle:nil] forCellReuseIdentifier:CLASS_NAME_FOR_TYPE(PostInfoTableViewCell)];
    [self.tableView registerNib:[UINib nibWithNibName:CLASS_NAME_FOR_TYPE(PostImageTableViewCell) bundle:nil] forCellReuseIdentifier:CLASS_NAME_FOR_TYPE(PostImageTableViewCell)];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Open in Safari"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(didTapOpenInSafari)];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.estimatedRowHeight = kPDTableViewPostInfoRowHeightBase;
}

- (void)didTapOpenInSafari {
    [[self postDisplayPresenter] didTapOpenInSafari];
}

#pragma mark - PostDisplayViewInterface

- (void)displayViewModel:(PostViewModel *)viewModel {
    self.viewModel = viewModel;
    [self.tableView reloadData];
}

@end

@interface PostDisplayViewController (TableViewDataSource) <UITableViewDataSource, UITableViewDelegate>
@end

@implementation PostDisplayViewController (TableViewDataSource)

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Post data";
    } else {
        return @"Post Images";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.viewModel.imagesURLs.count) {
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    } else {
        return self.viewModel.imagesURLs.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger section = indexPath.section;
    NSUInteger row = indexPath.row;
    if (section == 0) {
        PostInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME_FOR_TYPE(PostInfoTableViewCell) forIndexPath:indexPath];
        NSArray *titles = @[@"Title", @"Text", @"Date", @"URL"];
        NSArray *contents = @[SAFE(self.viewModel.title), SAFE(self.viewModel.subtitle), SAFE(self.viewModel.dateString), SAFE(self.viewModel.postURL)];
        cell.descriptionLabel.text = [titles safeObjectAtIndex:row];
        cell.contentLabel.text = [contents safeObjectAtIndex:row];
        return cell;
    } else {
        NSURL *url = [self.viewModel.imagesURLs safeObjectAtIndex:row];
        PostImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLASS_NAME_FOR_TYPE(PostImageTableViewCell) forIndexPath:indexPath];
        [cell setImageURL:url];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return UITableViewAutomaticDimension;
    } else {
        return kPDTableViewPostImageRowHeight;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section) {
        return kPDTableViewPostInfoRowHeightBase;
    } else {
        return kPDTableViewPostImageRowHeight;
    }
}

@end
