//
//  PostDisplayViewController.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BaseViewController.h"
#import "PostDisplayViewInterface.h"

@interface PostDisplayViewController : BaseViewController <PostDisplayViewInterface>

@end
