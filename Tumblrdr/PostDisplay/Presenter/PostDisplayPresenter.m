//
//  PostDisplayPresenter.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "RouterInterface.h"

#import "PostDisplayPresenter.h"
#import "PostDisplayViewInterface.h"
#import "Post.h"
#import "PostViewModel.h"
#import "PostMedia.h"

@interface PostDisplayPresenter ()

@property (strong, nonatomic) Post *post;
@end

@implementation PostDisplayPresenter

- (instancetype)initWithRouterInterface:(id<BaseRouterInterface>)routerInterface
                    interactorInterface:(id<BaseInteractorInterface>)interactorInterface
                                   post:(Post *)post {
    self = [self initWithRouterInterface:routerInterface interactorInterface:interactorInterface];
    if (self) {
        _post = post;
    }
    return self;
}

- (id<PostDisplayViewInterface>)postDisplayView {
    return AS_PROTOCOL(PostDisplayViewInterface, self.view);
}

- (id<RouterInterface>)postDisplayRouter {
    return AS_PROTOCOL(RouterInterface, self.router);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PostViewModel *viewModel = [PostViewModel new];
    viewModel.title = self.post.postTitle;
    viewModel.postURL = self.post.postURL;
    viewModel.subtitle = self.post.postSummaryText;
    NSArray *picMedia = [self.post.postMedia filteredArrayPassingTest:^BOOL(PostMedia *obj) {
        return (obj.mediaType == MediaTypeImage);
    }];
    NSArray *picURLs = [picMedia map:^id(PostMedia *obj) {
        return [NSURL URLWithString:obj.mediaURL];
    }];
    viewModel.imagesURLs = picURLs;
    NSDate *postDate = [NSDate dateWithTimeIntervalSince1970:self.post.unixTimestamp];
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterShortStyle;
    formatter.timeStyle = NSDateFormatterShortStyle;
    viewModel.dateString = [formatter stringFromDate:postDate];

    
    [[self postDisplayView] displayViewModel:viewModel];
}

#pragma mark - PostDisplayPresenterInterface implementation

- (void)didTapOpenInSafari {
    [[self postDisplayRouter] openURLInExternalApplication:[NSURL URLWithString:self.post.postURL]];
}

@end
