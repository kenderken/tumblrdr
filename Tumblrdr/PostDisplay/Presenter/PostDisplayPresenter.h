//
//  PostDisplayPresenter.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BasePresenter.h"
#import "PostDisplayPresenterInterface.h"
@class Post;

@interface PostDisplayPresenter : BasePresenter <PostDisplayPresenterInterface>

- (instancetype)initWithRouterInterface:(id<BaseRouterInterface>)routerInterface interactorInterface:(id<BaseInteractorInterface>)interactorInterface post:(Post *)post;

@end
