//
//  BasePresenter.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BasePresenter.h"

@interface BasePresenter ()

@property (strong, nonatomic) id<BaseRouterInterface>router;
@property (strong, nonatomic) id<BaseInteractorInterface>interactor;

@end

@implementation BasePresenter

- (instancetype)initWithRouterInterface:(id<BaseRouterInterface>)routerInterface interactorInterface:(id<BaseInteractorInterface>)interactorInterface {
    self = [self init];
    if (self) {
        _router = routerInterface;
        _interactor = interactorInterface;
    }
    return self;
}

- (void)setViewInterface:(id)viewInterface {
    _view = viewInterface;
}

- (void)viewDidLoad {
}

@end
