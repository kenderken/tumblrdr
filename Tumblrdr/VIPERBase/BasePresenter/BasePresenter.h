//
//  BasePresenter.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BaseInteractorInterface.h"
#import "BaseRouterInterface.h"
#import "BasePresenterInterface.h"
#import "BaseViewInterface.h"

@interface BasePresenter : NSObject <BasePresenterInterface>

- (instancetype)initWithRouterInterface:(id<BaseRouterInterface>)routerInterface interactorInterface:(id<BaseInteractorInterface>)interactorInterface;

@property (strong, nonatomic, readonly) id<BaseRouterInterface>router;
@property (strong, nonatomic, readonly) id<BaseInteractorInterface>interactor;

@property (weak, nonatomic) id<BaseViewInterface> view;

@end
