//
//  BasePresenterInterface.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

@protocol BasePresenterInterface <NSObject>

- (void)setViewInterface:(id)viewInterface;
- (void)viewDidLoad;

@end
