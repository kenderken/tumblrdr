//
//  BaseView.m
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@property (strong, nonatomic) id<BasePresenterInterface> presenter;

@end

@implementation BaseViewController

- (instancetype)initWithPresenterInterface:(id<BasePresenterInterface>)presenterInterface {
    NSString *nibName = NSStringFromClass([self class]);
    self = [self initWithNibName:nibName bundle:nil];
    if (self) {
        _presenter = presenterInterface;
        [_presenter setViewInterface:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.presenter viewDidLoad];
}



@end
