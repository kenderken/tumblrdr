//
//  BaseView.h
//  Tumblrdr
//
//  Created by Pawel Maczewski on 22/09/16.
//  Copyright © 2016 Pawel Maczewski. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewInterface.h"
#import "BasePresenterInterface.h"

@interface BaseViewController : UIViewController

- (instancetype)initWithPresenterInterface:(id<BasePresenterInterface>)presenterInterface;

@property (strong, nonatomic, readonly) id<BasePresenterInterface> presenter;

@end
